from pylab import *

RADIANS = 2 * pi
#MAX_RADIUS = 20


def radial_array(R, N, phase):
	angles = (2 * pi / N ) * (phase + arange(N))
	return vstack( (R * sin( angles ), R * cos( angles )) )  # x, y coords


from collections import deque


def occluding_angles( loc, points, diameter ):
	dx = points[0] - loc[0]
	dy = points[1] - loc[1]
	R = hypot(dx, dy)
	th = arctan2(dx, dy)

	dth = arcsin(diameter / R) / 2.0
	start = th - dth
	end = th + dth

	occ_order = argsort(start)
	start = start[occ_order]
	end = end[occ_order]
	spans = deque()
	L = start[0]
	R = end[0]
	for l, r in zip(start, end):
		if l > R:
			spans.append((L, R))
			L = l
			R = r
		if r > R:
			R = r

	while spans:
		l, r = spans[0]

		if (l % RADIANS) > (R % RADIANS):
			spans.append((L, R))
			break 				# no intersect
		else:
			spans.popleft()		# intersects; get rid of it
			R = r

	return (array(spans) % RADIANS )



def draw_poles(ax, loclist, r, **kwargs):
	for loc in loclist:
#		print loc
		ax.add_artist(mpl.patches.Circle(loc, r, **kwargs ))


def total_occlusion(loc, points, diameter ):
	spans = occluding_angles( loc, points, diameter )
	dth = diff(spans, axis=1)
	dth[dth < 0] += RADIANS
	return sum(dth) / RADIANS


def fix_rot(a):
	return (90 - rad2deg(a)) % 360


def draw_occlusion_angles(ax, center, pole_locs, pole_size, **kwargs):
#	print pole_locs
	maxrad = 1.5 * amax(fabs(ax.axis()))
	oa = occluding_angles(center, pole_locs, pole_size)
	for s, e in oa:
		ax.add_artist(mpl.patches.Wedge(center, maxrad, fix_rot(e), fix_rot(s), **kwargs))

if __name__ == "__main__":
	dot = radial_array(4, 18, 0.5)
	square = radial_array(2, 8, 0)
	pentagon = radial_array(3, 15, 0.5)
	many = radial_array(5, 22, 0)

	blob = hstack((dot, square, pentagon, many))

	o0 = occluding_angles( (0, 0), dot, 0.2 )
	o1 = occluding_angles( (0, 0), square, 0.2 )
	o2 = occluding_angles( (0, 0), pentagon, 0.2 )

	print total_occlusion(o1)
	#print dot
	#print square
	#print hstack((dot, square))
	fig, ax = plt.subplots()
	ax.set_xlim(-10, 10)
	ax.set_ylim(-10, 10)

	draw_poles(ax, blob.T, 0.2, ec='none', fc='black', zorder=3)
	draw_occlusion_angles(ax, (0, -1), blob, 0.2, ec='none', fc='blue', zorder=0 )

	# X, Y = meshgrid( linspace(-2, 2, 20), linspace(-2, 2, 20))
	# Z = zeros(prod(X.shape))
	# for e, loc in enumerate(zip(X.flat, Y.flat)):
	# 	Z[e] = total_occlusion(occluding_angles(loc, blob, 0.2))
	# Z.shape = X.shape
	# pcolor(X, Y, Z)

	show()
