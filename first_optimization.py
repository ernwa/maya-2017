"""greedy optimization of occlusion."""


from pylab import *
from first_calculations import \
	occluding_angles, total_occlusion, draw_poles, draw_occlusion_angles, radial_array

from scipy.optimize import differential_evolution

from sys import stderr, stdout

K_SHADE = 10

MAX_RINGS = 16

pole_diameter = 0.15
NOMINAL_SPACING = 1.0 + pole_diameter

ring_radii = (1.5 + arange(MAX_RINGS) * 1).astype(float32)
ring_poles = rint( (ring_radii * 2 * pi) / (0.8 + pole_diameter) ).astype(int32)
ring_shift = linspace(0, 1, MAX_RINGS)



class pole_ring(object):

	def __init__(self, rad_ring, diam_pole, n_poles, shift):
		self.rad_ring = rad_ring
		self.diam_pole = diam_pole
		self.n_poles = n_poles
		self.shift = shift
		self.recalculate()


	def recalculate(self):
		self.xy_poles = radial_array(self.rad_ring, self.n_poles, self.shift)


	def draw(self, xy_center, ax, **kwargs):
		draw_poles(ax, self.xy_poles.T, self.diam_pole, ec='none', fc='#FF0037', zorder=3)
		ax.add_artist(mpl.patches.Circle((0, 0), self.rad_ring, ec='#757778', fc='none', zorder=2 ))
		draw_occlusion_angles(ax, xy_center, self.xy_poles, self.diam_pole, ec='none', fc='#A3C6D1', zorder=1)


	def __repr__(self):
		return "pole_ring( %f, %f, %d, %f )" % (self.rad_ring, self.diam_pole, self.n_poles, self.shift)



if __name__ == "__main__":

	stderr.write( 'optimizing rings greedily \n' )

	XY_CENTER = (0, 0)

	TEST_LOCS = radial_array(2, 3, 0.2).T
#	stderr.write( TEST_LOCS



	rings = [ pole_ring(r, pole_diameter, n, s) for (r, n, s) in zip(ring_radii, ring_poles, ring_shift) ]


	def unshaded(i):
		u = 0
		for loc in TEST_LOCS:
			u += 1.0 - total_occlusion(
					loc,
					hstack([r.xy_poles for r in rings[:i + 1]]),
					pole_diameter)
		return u / len(TEST_LOCS)


	def objective_func( parms, i, prev_score):
		shift, dr, dp = parms

		nominal_rad = rings[i - 1].rad_ring + NOMINAL_SPACING

		rings[i].shift = clip(shift, 0, 1.0)
		rings[i].rad_ring = nominal_rad + clip(dr, -0.5, 0.2)
		rings[i].n_poles = rint( (nominal_rad * pi * 2) / ( NOMINAL_SPACING + clip( dp, -0.2, 3 )  ) )
		rings[i].recalculate()

		return K_SHADE * (unshaded(i) - prev_score)


	for i in range(1, MAX_RINGS):
		prev_score = unshaded(i)

		result = differential_evolution(
			objective_func,
			bounds=[ (0, 1.0), (-0.5, 0.2), (-0.2, 3) ],
			polish=True,
			init='random',
			args=(i, prev_score)
		)

		shift, dr, dp = result.x

		nominal_rad = rings[i - 1].rad_ring + NOMINAL_SPACING

		rings[i].shift = clip(shift, 0, 1.0)
		rings[i].rad_ring = nominal_rad + clip(dr, -0.2, 0.2)
		rings[i].n_poles = rint( (nominal_rad * 2 * pi) / (NOMINAL_SPACING + clip( dp, -0.2, 0.2 ) ) )
		rings[i].recalculate()



		stderr.write( 'optimizing ring %d. %d elements %4.2f m radius. score: %3.2f, ' % ( i, rings[i].n_poles, rings[i].rad_ring, result.fun ) )

#		occlusion = total_occlusion( XY_CENTER, hstack([r.xy_poles for r in rings[:i + 1]]), rings[0].diam_pole )  # fixme: diam_pole vary?
		stderr.write( '%f \n' % (prev_score,)) 



	stderr.write('found solution occluding %4.1f%% :\n\n' % (unshaded(MAX_RINGS-1) * 100))

	stdout.write('ring\tradius\tn_poles\tshift\n')

	n_poles = 0
	for i, r in enumerate(rings):
		stdout.write('%d\t%4.3f\t%d\t%4.3f\n' % (i, r.rad_ring, r.n_poles, r.shift))
		n_poles += r.n_poles

	stdout.write('\n#poles total: %d\n' % n_poles)

	def draw_figure(xy_center, rings):
		fig = figure()
		ax = fig.add_subplot('111', aspect='equal')
		ax.set_xlim(-40, 40)
		ax.set_ylim(-40, 40)

		for r in rings:
			r.draw(xy_center, ax)

		# X, Y = meshgrid( linspace(-2, 2, 20), linspace(-2, 2, 20))
		# Z = zeros(prod(X.shape))
		# for e, loc in enumerate(zip(X.flat, Y.flat)):
		# 	Z[e] = total_occlusion(occluding_angles(loc, blob, 0.2))
		# Z.shape = X.shape
		# pcolor(X, Y, Z)

		show()

	draw_figure((0, 0), rings)
	draw_figure((0, 1), rings)
