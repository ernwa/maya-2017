"""greedy optimization of occlusion."""

from sys import exit
from pylab import *
from first_calculations import \
	occluding_angles, total_occlusion, draw_occlusion_angles, radial_array

from scipy.spatial import KDTree

from scipy.optimize import differential_evolution, basinhopping


def soft_clip(x, l, h):
	t = tanh((x - l) / (h - l) - 0.5)
	return (t + 0.5) * (h - l) + l

def unshaded( locs, poles, diam_poles):
	unshaded = 0
	for loc in locs:
		unshaded += 1.0 - total_occlusion( loc, poles, diam_poles )
	return unshaded / len(locs)
# figure()
# X = linspace(-10, 20, 50)
# Y = soft_clip(X, 0, 2*pi)
# plot(X,Y)
# plot(X,X)
# show()
# import sys
# sys.exit()

if __name__ == "__main__":

	print 'greedy algorithm; placing poles one by one'

	K_DIST = 2
	K_SHADE = 100
	MAX_POLES = 700
	DIAM_POLES = 0.07

	INNERMOST_POLES = radial_array(1.5, 10, 0)
	MAX_POLES -= INNERMOST_POLES.shape[1]

#	TEST_LOCS = radial_array(2, 3, 0.2).T
	TEST_LOCS = [(0, 0)]
	print TEST_LOCS

	r_max = 1.5	 # each point added will increase this
	pole_locs = INNERMOST_POLES




	def objective_func( parms, last_unshaded, poles, poletree):
		r, th = parms
	#	r = soft_clip(r, 1.5, 20)
	#	th = th % (2 * pi)
		pt = (r * sin(th), r * cos(th))
		poles[:, -1] = pt

		cur_unshaded = unshaded(TEST_LOCS, poles, DIAM_POLES)

		mindist = clip( 0.9 - poletree.query(pt)[0], 0, 1 )

		return K_SHADE * (cur_unshaded - last_unshaded) + K_DIST * mindist

	r = 2.5
	th = 0


	for i in range(MAX_POLES):
		tmp_poles = hstack((pole_locs, [[0], [0]]))
		tmp_tree = KDTree(pole_locs.T)

	# 	X,Y = meshgrid(linspace(-5,5,80), linspace(-5,5,80))
	# 	r = hypot(X,Y)
	# 	th = arctan2(X,Y)
	# 	valid_mask = (r > 0.5) * (r < 20)
	# 	R = r[valid_mask]
	# 	TH = th[valid_mask]

	# 	I = nonzero(valid_mask)
	# 	print R.shape

	# 	Z = zeros_like(X) 
	# 	Z[I] = [ objective_func( (rr, tth), 0.93, tmp_poles, tmp_tree) for rr, tth in zip(R,TH) ]
	# #	print Z
	# 	fig = figure()
	# 	ax = fig.add_subplot('111', aspect='equal')
	# 	pcolor(X, Y, Z, cmap='gnuplot',vmin=Z.min(), vmax=Z.max())
	# 	colorbar()
	# 	for p in pole_locs.T:
	# 		ax.add_artist(mpl.patches.Circle( p, DIAM_POLES / 2, ec='none', fc='black', zorder=2 ))
	# 	show()

	# 	exit()

		last_unshaded = unshaded(TEST_LOCS, pole_locs, DIAM_POLES)

		result = differential_evolution(
			objective_func,
			bounds=[ (1.5, 20.0), (0.0, 2 * pi) ],
			tol=1e-2,
			popsize=15,
			mutation=(0.5, 1.0),
			polish=True,
			args=( last_unshaded, tmp_poles, tmp_tree) )

		# result = basinhopping(
		# 	objective_func,
		# 	x0=[ r, th + (1 / r) ],
		# 	stepsize=0.5,
		# 	minimizer_kwargs={'args': (r_max, tmp_poles, tmp_tree)},
		# 	interval=50,
		# 	niter_success=2 )


		r, th = result.x
#		r = soft_clip(r, 1.5, 20)
#		th = th % (2*pi)
		tmp_poles[:, -1] = (r * sin(th), r * cos(th))
		pole_locs = tmp_poles.copy()

		print pole_locs.shape[0], 'r=', r, 'th=', th, 'closest=', tmp_tree.query(tmp_poles[:, -1])[0]

		print 'placed pole', i, 'score =', result.fun, 'occlusion =', total_occlusion( (0, 0), pole_locs, DIAM_POLES)
	#	print result.message

	for i, x in enumerate(pole_locs.T):
		print '%4d: %6.3f, %6.3f' % (i, x[0], x[1])

	def draw_figure(xy_center, pole_locs):
		fig = figure()
		ax = fig.add_subplot('111', aspect='equal')
		ax.set_xlim(-40, 40)
		ax.set_ylim(-40, 40)

		for p in pole_locs.T:
			ax.add_artist(mpl.patches.Circle( p, DIAM_POLES / 2, ec='none', fc='#FF0037', zorder=2 ))


		draw_occlusion_angles(ax, xy_center, pole_locs, DIAM_POLES, ec='none', fc='#A3C6D1', zorder=0)

		# X, Y = meshgrid( linspace(-2, 2, 20), linspace(-2, 2, 20))
		# Z = zeros(prod(X.shape))
		# for e, loc in enumerate(zip(X.flat, Y.flat)):
		# 	Z[e] = total_occlusion(occluding_angles(loc, blob, 0.2))
		# Z.shape = X.shape
		# pcolor(X, Y, Z)

		show()

	draw_figure((0, 0), pole_locs)
	draw_figure((0, 1), pole_locs)
